__author__ = 'Yuki YONEZAWA'

class FlyBehavior(object):
    def fly(self):
        pass

class FlyWithWings(object):
    def fly(self):
        print "I can fly"

class FlyNoWay(object):
    def fly(self):
        print "I cannot fly"

class QuackBehavior(object):
    def quakc(self):
        pass
class Quack(object):
    def quack(self):
        print "Duck is quack"

class Squeak(object):
    def quack(self):
        print "Kyu- Kyu-"

class MutaQuack(object):
    def quack(self):
        print ""

class Duck(object):

    def __init__(self):
        self.flyBehavior = None
        self.quackBehavior = None

    def setFlyBehavior(self, aFlyBehavior):
        self.flyBehavior = aFlyBehavior

    def setQuackBehavior(self, aQuackBehavior):
        self.quackBehavior = aQuackBehavior

    def performQuack(self):
        self.quackBehavior.quack()

    def performFly(self):
        self.flyBehavior.fly()

