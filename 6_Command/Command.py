__author__ = 'yuki'
import abc
class Command(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def execute(self):
        raise NotImplementedError

class Light(object):
    def on(self):
        print "Light is on!"

    def off(self):
        print "Light is off!"

class LightOnCommand(Command):
    def __init__(self, aLight):
        self.light = aLight

    def execute(self):
        self.light.on()

class SimpleRemoteControl(object):
    def __init__(self):
        self.slot = None

    def setCommand(self, command):
        self.slot = command

    def buttonWasPressed(self):
        self.slot.execute()

if __name__ == "__main__":
    control = SimpleRemoteControl()
    light = Light()
    lightOnCommand = LightOnCommand(light)
    control.setCommand(lightOnCommand)
    control.buttonWasPressed()
