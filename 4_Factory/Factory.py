from _ast import Not

__author__ = 'yuki'

import abc
class PizzaIngredientFactory(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def createDough(self):
        raise NotImplementedError

    @abc.abstractmethod
    def createSauce(self):
        raise NotImplementedError

    @abc.abstractmethod
    def createCheese(self):
        raise NotImplementedError

    @abc.abstractmethod
    def createVeggies(self):
        raise NotImplementedError

    @abc.abstractmethod
    def createPepperoni(self):
        raise NotImplementedError

    @abc.abstractmethod
    def createClam(self):
        raise NotImplementedError

class ThinCrustDough(object):
    pass
class MarineraSauce(object):
    pass
class ReggianoCheese(object):
    pass
class Garlic(object):
    pass
class Onion(object):
    pass
class Mushroom(object):
    pass

class RedPepper(object):
    pass

class SlicedPepperoni(object):
    pass
class FreshClams(object):
    pass

class NYPizzaIngredientFactory(PizzaIngredientFactory):
    def createDough(self):
        return ThinCrustDough()

    def createSauce(self):
        return MarineraSauce()

    def createCheese(self):
        return ReggianoCheese()

    def createVeggies(self):
        return [Garlic(), Onion(), Mushroom(), RedPepper()]

    def createPepperoni(self):
        return SlicedPepperoni()

    def createClam(self):
        return FreshClams()

class Pizza(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.name = ""
        self.dough = None
        self.sauce = None
        self.veggies = []
        self.cheese = None
        self.pepperoni = None
        self.clam = None

    @abc.abstractmethod
    def prepare(self):
        raise NotImplementedError

    def bake(self):
        print "bake 25 minutes 350"

    def cut(self):
        print "cut pizza"

    def box(self):
        print "insert to box "

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def toString(self):
        print "pizza code"

class CheesePizza(Pizza):

    def __init__(self, ingredientFactory):
        self.ingredientFactory = ingredientFactory

    def prepare(self):
        print "do "+self.name
        self.dough = self.ingredientFactory.createDough()
        self.sauce = self.ingredientFactory.createSauce()
        self.cheese = self.ingredientFactory.createCheese()

class ClamPizza(Pizza):
    def __init__(self, ingredientFactory):
        self.ingredientFactory = ingredientFactory

    def prepare(self):
        print "do "+self.name
        self.dough = self.ingredientFactory.createDough()
        self.sauce = self.ingredientFactory.createSauce()
        self.cheese = self.ingredientFactory.createCheese()
        self.clam = self.ingredientFactory.createClam()

class NYPizzaStore(object):

    def createPizza(self, item):
        pizza = None
        ingredientFactory = PizzaIngredientFactory()
        if item == "cheese":
            pizza = CheesePizza(ingredientFactory)
            pizza.setName("NY Style Cheese Pizza")
        elif item == "clam":
            pizza = ClamPizza(ingredientFactory)
            pizza.setName("NY Style Clam Pizza")

        return pizza