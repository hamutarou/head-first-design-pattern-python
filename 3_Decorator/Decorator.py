__author__ = 'yuki'

import abc
class Beverage(object):
    __metaclass__ = abc.ABCMeta

    def getDescription(self):
        return self.description

    @abc.abstractmethod
    def cost(self):
        pass

class CondimentDecorator(Beverage):
    pass

class Espresso(Beverage):
    def __init__(self):
        self.description = "espresso"

    def cost(self):
        return 1.99

class HouseBlend(Beverage):

    def __init__(self):
        self.description = "house blend"

    def cost(self):
        return 0.89

class Mocha(CondimentDecorator):
    def __init__(self, aBeverage):
        self.beverage = aBeverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Mocha"

    def cost(self):
        return 0.20 + self.beverage.cost()

class Whip(CondimentDecorator):
    def __init__(self, aBeverage):
        self.beverage = aBeverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Whip"

    def cost(self):
        return 0.5 + self.beverage.cost()

class Soy(CondimentDecorator):
    def __init__(self, aBeverage):
        self.beverage = aBeverage

    def getDescription(self):
        return self.beverage.getDescription() + ", Soy"

    def cost(self):
        return 0.9 + self.beverage.cost()

if __name__ == "__main__":
    espresso = Espresso()
    espresso = Mocha(espresso)
    espresso = Soy(espresso)
    print espresso.getDescription(), espresso.cost()

    houseblend = HouseBlend()
    houseblend = Whip(houseblend)
    print houseblend.getDescription(), houseblend.cost()