from _ast import Not
__author__ = 'yuki'
import abc

class Subject(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def registerObserver(self, Observer):
        raise NotImplementedError

    @abc.abstractmethod
    def removeObserver(self, Observer):
        raise NotImplementedError


    @abc.abstractmethod
    def notifyObserver(self):
        raise NotImplementedError

class Observer(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def update(self, temperature, humidity, pressure):
        raise NotImplementedError

class DisplayElement(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def display(self):
        raise NotImplementedError

class WeatherData(Subject):
    def __init__(self):
        self.observers = []
        self.temperature = 0.0
        self.humidity = 0.0
        self.pressure = 0.0

    def registerObserver(self, aObserver):
        self.observers.append(aObserver)

    def removeObserver(self, aObserver):
        self.observers.remove(aObserver)

    def notifyObserver(self):
        for observer in self.observers:
            observer.update(self.temperature, self.humidity, self.pressure)

    def measurementsChanged(self):
        self.notifyObserver()

    def setMeasurements(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.measurementsChanged()

class CurrentConditionsDisplay(Observer, DisplayElement):
    def __init__(self, aWeatherData):
        self.temperature = 0.0
        self.humidity = 0.0
        self.weatherData = aWeatherData
        self.weatherData.registerObserver(self)

    def update(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.display()

    def display(self):
        print "T:%f, H:%f" % (self.temperature, self.humidity)

if __name__ == "__main__":
    weatherData = WeatherData()
    display = CurrentConditionsDisplay(weatherData)


    weatherData.setMeasurements(28, 10, 130.0)
    weatherData.setMeasurements(18, 11, 123.0)
    weatherData.setMeasurements(27, 19, 110.0)