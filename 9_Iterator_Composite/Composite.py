__author__ = 'yuki'

import abc
class MenuComponent(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def add(self, menuComponent):
        raise NotImplementedError

    @abc.abstractmethod
    def remove(self, menuComponent):
        raise NotImplementedError

    @abc.abstractmethod
    def getChild(self, i):
        raise NotImplementedError

    @abc.abstractmethod
    def getName(self):
        raise NotImplementedError

    @abc.abstractmethod
    def getDescription(self):
        raise NotImplementedError

    @abc.abstractmethod
    def getPrice(self):
        raise NotImplementedError

    @abc.abstractmethod
    def getIsVegetarian(self):
        raise NotImplementedError

    @abc.abstractmethod
    def show(self):
        raise NotImplementedError

class MenuItem(MenuComponent):
    def __init__(self, name, description, isVegetarian, price):
        self.name = name
        self.description = description
        self.isVegetarian = isVegetarian
        self.price = price

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def getIsVegetarian(self):
        return self.isVegetarian

    def getPrice(self):
        return self.price

    def show(self):
        print "Name:%s V:%s P:$%d %s" % ( self.getName(), self.getIsVegetarian(), self.getPrice(), self.getDescription())

class Menu(MenuComponent):
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.menuComponents = []

    def add(self, menuComponent):
        self.menuComponents.append(menuComponent)

    def remove(self, menuComponent):
        self.menuComponents.remove(menuComponent)

    def getChild(self, i):
        return self.menuComponents[i]

    def getName(self):
        return self.name

    def getDescription(self):
        return self.description

    def show(self):
        print "N:%s D:%d", %(self.getName(), self.getDescription())
        print "---------------------"

        for menuComponent in self.menuComponents:
            menuComponent.show()

class Waiter(object):
    def __init__(self, allMenu):
        self.allMenus = allMenu

    def showMenu(self):
        self.allMenus.show()


if __name__ == "__main__":

    pancakeHouseMenu = Menu("Pancake House Menu", "Morning")
    dinerMenu = Menu("Diner Menu", "Lunch")
    cafeMenu = Menu("Cafe Menu", "Dinner")
    dessertMenu = Menu("Dessert Menu", "After Eating")

    allMenu = Menu("All Menu", "all menu")
    allMenu.add(pancakeHouseMenu)
    allMenu.add(dessertMenu)
    allMenu.add(cafeMenu)
    allMenu.add(dessertMenu)

    dinerMenu.add(MenuItem("pasta", "manara sauce and saward bread", True, 3.89))
    dessertMenu.add(MenuItem("apple pie", "Apple and Pie", True, 1.89))

    waiteress = Waiter(allMenu)
    waiteress.showMenu()